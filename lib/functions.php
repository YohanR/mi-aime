<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent() {
	if(isset($_GET['home'])) {
		include __DIR__.'/../pages/home.php';
	} if(isset($_GET['annuaire'])) {
		include __DIR__.'/../pages/annuaire.php';
	} if(isset($_GET['contact'])) {
		include __DIR__.'/../pages/contact.php';
	} if(empty($_GET)) {
		include __DIR__.'/../pages/home.php';
	} if(isset($_GET['preparer'])) {
		include __DIR__.'/../pages/preparer.php';
	} if(isset($_GET['save'])) {
		include __DIR__.'/../public/save.php';
	}
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

function getUserData() {
	$file = file_get_contents("../data/user.json");
	$user =	json_decode($file);
	echo $user->name . " " . $user->first_name . " " . $user->occupation . "</br>";
	foreach($user->experiences as $value){
	  echo $value->company . " : " . $value->year . "</br>" ;
	
	
	}
	
	
}
function lastMessage() {
	$data = file_get_contents('../data/last_message.json');
	$dataJson = json_decode($data, true);
	$name = $dataJson['user_name'];
	$mail = $dataJson['user_mail'];
	$message = $dataJson['user_message'];
	echo "<div class='lastMessage'> De la part de  $name  <br>  $mail  <br>  Message : $message <br> <a href='/?home'>Cliquer ici pour retourner a l'acceuil !</a> </div>";
	
}

