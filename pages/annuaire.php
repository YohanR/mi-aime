<div class="centre">
<h3>Hôtels de La Réunion</h3>
  </div>

<div class="row text-center gap">
  <div class="col-md-4 col-12">
        <img src="images/hotel1.jpg" class="img-fluid" max-width="100%" height="auto" max-height="348 px" alt="...">
 </div>
  <div class="col-md-4 col-12">
        <img src="images/hotel2.jpg" class="img-fluid" max-width="100%" height="auto" max-height="348 px" alt="...">
 </div>
  <div class="col-md-4 col-12">
        <img src="images/hotel3.jpg" class="img-fluid" max-width="100%" height="auto" max-height="348 px" alt="...">
 </div>
</div>
<br><br>
<div class="row">
  <div class="col-md-6 col-12">
    <h2>Hôtels secteur Nord de La Réunion :</h2><br>
    <p><h3>Saint-Denus :</h3>  Hôtels Saint-Denis :: Hôtels Bellepierre :: Hôtels Montgaillard :: Hôtels Saint-François :: Hôtels à Sainte-Clotilde<br><br>
      <h3>La Possession :</h3> Hôtels La Possession<br><br>
      <h3>Sainte-Marie :</h3> Hôtels Sainte-Marie<br> <br>
      <h3>Sainte-Suzanne :</h3> Hôtels Sainte-Suzanne<br> 
    </p>
  </div>
  <div class="col-md-6 col-12">
    <h2>Hôtels secteur Sud de La Hôtels Saint-Pierre :</h2><br>
    <p><h3> Hôtel Grand-Bois :</h3> Hôtels à Montvert :: Hôtels à Pierrefonds :: Hôtels Ravine des Cabris<br><br><br>
      <h3>Etang-Salé :</h3> Hôtels Étang-Salé<br><br>
      <h3>Les Avirons :</h3>  Hôtels Les Avirons<br> <br>
      <h3>Le Tampon :</h3> Hôtels Le Tampon<br> 
    </p>
  </div>
</div>
<br><br>
<div class="row">
  <div class="col-md-6 col-12">
    <h2>Hôtels secteur Ouest de La Réunion :</h2><br>
    <p><h3>Saint-Gilles :</h3>   Hôtels Saint-Gilles les Bains :: Hôtels Boucan Canot :: Hôtels La Saline :: Hôtels Hermitage :: Hôtels Saint-Gilles les Hauts<br><br>
      <h3>Saint-Leu :</h3> Hôtels Saint-Leu :: Hôtels Pointe des Châteaux :: Hôtels La Pointe au sel Les Hauts<br><br>
    </p>
  </div>
  <div class="col-md-6 col-12">
    <h2>Hôtels secteur Est de La Réunion :</h2><br>
    <p><h3> Saint-Benoît </h3> Hôtels Saint-Benoît et Sainte-Anne<br><br><br>
      <h3>Plaine des Palmistes :</h3>Hôtels Plaine des Palmistes<br><br>
    </p>
  </div>
</div>





</div>