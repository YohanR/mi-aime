
<div class="centre">
<h3>Présentation de la Réunion</h3>
  </div>
<div class="row">
  <div class="col-md-6 col-12">
  <br>
    <h3>Mais ou est donc situé l'ile de la Réunion?</h3><br>
    <p>Située dans l’océan Indien, à 800 km à l’est de Madagascar, l’île de La Réunion, au climat tropical, constitue, avec les îles Maurice et Rodrigues, l’archipel des Mascareignes.</p><br>

        <img src="images/carte1.png" class="img-fluid" max-width="100%" height="auto" max-height="348 px" alt="...">

  </div>
  <div class="col-md-6 col-12">
<br>
    <h3>La Réunion est constituée de deux ensembles volcaniques</h3><p>- Dans la partie Nord-Ouest, le Piton des Neiges 3 070,50 mètres domine les trois cirques de Cilaos, Salazie et Mafate qui l’enserrent.<br>

- Au sud-est, le Piton de la Fournaise 2 631 mètres est un volcan actif, de type hawaïen. Il s’agit d’un volcan particulièrement actif.</p>

     <img src="images/volcan.jpg" class="img-fluid" max-width="100%" height="auto" max-height="348 px"  alt="...">

  </div>
</div>
<br><br>
<div class="row">
  <div class="col-md-6 col-12">
  <br>
    <h3>Climat de La Réunion.</h3><br>
    <p>Le climat de La Réunion, de type tropical, est marqué par l’influence des vents alizés. La côte orientale de l’île, dite "côte au vent", très arrosée, où plusieurs rivières, comme celles du Mât, des Marsouins, de l’Est, sont pérennes, contraste avec la côte occidentale, "sous le vent", aux terres arides. La saison des cyclones dure de décembre à avril.</p><br>
  </div>
  <div class="col-md-6 col-12">
<br>
    <h3>Population de La Réunion.</h3><br><p>La population de La Réunion, avec 833 944 habitants (2012), avec une densité de 332 habitants/km², est la plus importante de tout l’Outre-Mer.

Étant donné la présence de différentes ethnies au sein de la population réunionnaise, d'autres langues sont présentes sur l'île comme le hakka, le cantonais, le gujarati, l'ourdou, l'arabe, le tamoul, le malgache, le mahorais et le comorien
</p>
  </div>
</div>
<br><br>
<div class="row">
  <div class="col-md-12 col-12">
    <br>
    <h3 class="text-center">La Réunion département français.</h3><br>
    <p>La Réunion, département français, est placée sous l’autorité d’un préfet nommé par le Gouvernement.

L’île, devenue département français depuis la loi du 19 mars 1946, est dotée d’un conseil régional et d’un conseil général. Tous les textes nationaux y sont applicables. Seules certaines adaptations ont été prévues par la loi.

La Réunion comprend 24 communes et 47 cantons. Elle est représentée par 7 députés et 4 sénateurs au Parlement et par un conseiller au Conseil Économique et Social.

La préfecture est située à Saint-Denis et 3 sous-préfectures à Saint-Pierre, Saint-Paul et Saint-Benoît.

En tant que département français d’Outre-Mer, La Réunion fait partie de l’Union Européenne au sein de laquelle elle constitue une région ultra-périphérique ; à ce titre, elle bénéficie de "mesures spécifiques" qui adaptent le droit communautaire en tenant compte des caractéristiques et contraintes particulières de ces régions.

La Réunion accueille à Saint-Pierre le siège des Terres australes et antarctiques françaises (Taaf).</p><br>
  </div>
</div>
<br><br>
<div class="row">
  <div class="col-md-12 col-12 text-center">
    <br>
    <h3 class="text-center">Vidéo découverte Ile de la Réunion</h3><br>
    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/1fiNYk8mSpo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
</div>
