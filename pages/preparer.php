<br> <br>
<div class="row">
  <div class="col-md-12 col-12">
    <h2>La meilleure période pour partir</h2><br>
    <p>L’île de la Réunion est située dans l’hémisphère sud, de ce fait les saisons sont inversées par rapport à la métropole. L’été de novembre à avril est une saison chaude et humide. L’hiver de mai à octobre est plutôt sec et tempéré.
De Mai à Octobre-Novembre il fait un peu plus frais, et le reste de l’année il fait chaud ! Donc si vous souhaitez partir en randonnée et atteindre le fougueux et somptueux Volcan Piton de la Fournaise, il est préférable de privilégier l’hiver austral. Si vous êtes à la recherche de vitamine D, direction la côte Ouest, où les précipitations sont rares.<br><br>

Si votre envie est de profiter des magnifiques fonds sous-marins ou juste de glisser sur les immenses vagues de l’île, dans ce cas vous avez l’embarras du choix en terme de date, excepté la période de décembre à mars, un petit plus agitée.<br><br>

Ce qu’il est important de savoir toutefois, c’est que le climat à la Réunion est relativement doux toute l’année, et les températures de l’eau sont comprises entre 22 et 27°C toute l’année. Donc si vous avez juste envie de vacances, vous pouvez partir à la dernière minute sans aucun soucis.
    </p><br><br>
  </div>
  <div class="row">
  <div class="col-md-6 col-12">
    <h2>La période idéale pour visiter ?</h2><br>
    <p>Sans hésiter, partez pendant la saison sèche, durant les mois de Septembre, Octobre et Novembre où le climat est idéal pour découvrir les magnifiques paysages verdoyants.
  </div>
  <div class="col-md-6 col-12">
    <h2>Quand partir pour profiter de ses plages ?</h2><br>
    <p>Si vous vous rendez sur l’île de la Réunion , pour profiter un maximum de ses plages et de son climat ensoleillé, alors réservez votre voyage entre Mai et Novembre.
    </p><br><br>
  </div>
    <div class="row">
  <div class="col-md-6 col-12">
    <h2>Je mets quoi dans ma valise ?</h2><br>
    <p>Pour être certain de ne rien oublier dans votre valise, suivez nos conseils pour partir à la Réunion l'esprit tranquille. Prévoyez des vêtements légers (en coton de préférence) ainsi que votre maillot de bain, des tenues décontractées et des shorts pour la journée au bord de la plage ainsi que des sandales en plastiques pour éviter de se couper sur des rochers.<br><br>

Pour le soir, pensez à prendre une petite laine, car la climatisation des hôtels peut être fraîche, tout comme les nuits. Pendant l’hiver austral, de mai à octobre, les températures le soir et plus particulièrement sur les hauteurs peuvent être fraîches.<br><br>

Pour les amateurs de randonnées et promenades, un sac à dos, des tenues de marche adaptées et des baskets ou chaussures de marche sont indispensables. De plus un coupe vent sera le bienvenu (les températures refroidissent avec l'altitude).<br><br>

Pensez également à prendre votre crème solaire avec un indice UV adapté à votre peau pour vos journées baignade et farniente. Un spray anti moustiques est conseillé pendant la saison humide.</p>
  </div>
  <div class="col-md-6 col-12">
    <h2>Quand partir pour profiter de ses plages ?</h2><br>
    <p>Si vous vous rendez sur l’île de la Réunion , pour profiter un maximum de ses plages et de son climat ensoleillé, alors réservez votre voyage entre Mai et Novembre.
    </p>
  </div>


