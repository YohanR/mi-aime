
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #FFFF6F;">
  <a class="navbar-brand" href="?home">La Réunion</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="?home">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?annuaire">Annuaire touristique</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?preparer">Préparer son voyage</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?contact">Contact</a>
      </li>
    </ul>
  </div>
</nav>